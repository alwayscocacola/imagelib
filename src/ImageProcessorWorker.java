/**
 * Worker class.
 */

import org.apache.log4j.Logger;

import java.io.File;


public class ImageProcessorWorker implements Runnable {

	private static final Logger logger = Logger.getLogger(ImageProcessorWorker
			.class);

	private final ImageProcessor imageProcessor;

	public ImageProcessorWorker(File image) {
		this.imageProcessor = new ImageProcessor(image);
	}

	@Override
	public void run() {
		long threadID = Thread.currentThread().getId();
		logger.info("ImageProcessorWorker#" + threadID);
		imageProcessor.save(imageProcessor.resize(ImageSettings.MEDIUM_WIDTH,
				ImageSettings.MEDIUM_HEIGHT), ImageSettings.IMG_MEDIUM_SIZE_PATH);
		boolean flag = imageProcessor.save(imageProcessor.resize(ImageSettings
				.BIG_THUMBNAIL_WIDTH, ImageSettings.BIG_THUMBNAIL_HEIGHT),
				ImageSettings.IMG_BIG_THUMBNAIL_PATH) &&
				imageProcessor.save(imageProcessor.resize(ImageSettings
						.MEDIUM_THUMBNAIL_WIDTH, ImageSettings.MEDIUM_THUMBNAIL_HEIGHT),
						ImageSettings.IMG_MEDIUM_THUMBNAIL_PATH) && imageProcessor.save(
				imageProcessor.resize(ImageSettings.SMALL_THUMBNAIL_WIDTH,
						ImageSettings.SMALL_THUMBNAIL_HEIGHT),
				ImageSettings.IMG_SMALL_THUMBNAIL_PATH);
		if (flag) {
			String filename = imageProcessor.getFilename();
			logger.info("ImageProcessorWorker: Filename - " + filename);
			imageProcessor.delete();
		}
	}
}
