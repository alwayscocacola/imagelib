/**
 * This class resizes the image.
 */

import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLConnection;

public class ImageProcessor {
	private final static Logger logger = Logger.getLogger(ImageProcessor.class);

	private final File image;
	private final BufferedImage img;
	private final String filename;

	public ImageProcessor(File image) {
		this.image = image;
		this.img = this.load();
		this.filename = String.valueOf(ImageFileNamerHash.generateHash());
		logger.info("ImageProcess: " + image.getName());
	}

	public String getFilename() {
		return filename;
	}

	/**
	 * Get file format. We don't guess the filetype by reading mimetype. Just
	 * take the rest of the string from starting the last dot.
	 * @return Image type or null.
	 */
	private String getFileFormat() {
		String imageName = image.getName();
		final int val = imageName.lastIndexOf('.');
		final int LENGTH = imageName.length();
		if (val > 0 && val < LENGTH - 1) {
			return imageName.substring(val + 1);
		}
		return null;
	}

	/**
	 * Reads the image.
	 * @return Buffered image.
	 */
	private BufferedImage load() {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(image);
			logger.info("ImageProcess.load() - " + image.getName());
		} catch (IOException e) {
			logger.debug(e.getMessage());
		}
		return temp;
	}

	/**
	 * Saves the image to the given path.
	 * @param img Buffered image.
	 * @param path Given path to save the image.
	 * @return True or false. If we can't save the image then it returns false.
	 */
	public boolean save(BufferedImage img, String path) {
		boolean flag;
		try {
			final String ABSPATH = path + filename + "." + getFileFormat();
			ImageIO.write(img, getFileFormat(), new File(ABSPATH));
			flag = true;
			logger.info("ImageProcess.save() - " + path + " " + getFileFormat());
		} catch (IOException e) {
			flag = false;
			logger.debug(e.getMessage());
		}
		return flag;
	}

	/**
	 * Once the image has been saved, the original image is deleted from its
	 * original path.
	 * @return True if it's deleted successfully.
	 */
	public boolean delete() {
		return this.image.delete();
	}

	/**
	 * Resizes the image with the given width and height.
	 * @param width Maximum width of the image.
	 * @param height Maximum height of the image.
	 * @return Buffered image.
	 */
	public BufferedImage resize(int width, int height) {
		logger.info("ImageProcess.resize() - " + width + " " + height);
		double w = (double) width / (double) img.getWidth();
		double h = (double) height / (double) img.getHeight();
		double scale = w < h ? w : h;
		BufferedImage tmp = new BufferedImage((int) (img.getWidth() * scale),
				(int) (img.getHeight() * scale), img.getType());
		Graphics2D g = tmp.createGraphics();
		g.drawImage(img, 0, 0, tmp.getWidth(), tmp.getHeight(), null);
		g.setComposite(AlphaComposite.Src);
		g.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
				RenderingHints.VALUE_INTERPOLATION_BILINEAR);
		g.setRenderingHint(RenderingHints.KEY_RENDERING,
				RenderingHints.VALUE_RENDER_QUALITY);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g.dispose();
		return tmp;
	}
}
