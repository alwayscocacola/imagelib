import java.util.UUID;

public class ImageFileNamerHash {

	private static final int PRIME_32B = 16777619;
	private static final long OFFSET_BASIS_32B = 2166136261L;
	private static final int FOLD = 32;
	private static final int MASK = 0xCAFEBABE;

	/**
	 * Generates a hash value for the given string, fold and mask values.
	 * @param s
	 * @param fold
	 * @param mask
	 * @return Hash value.
	 */
	protected static long generateHash(String s, int fold, int mask) {
		long hashval = OFFSET_BASIS_32B;
		final int LENGTH = s.length();
		for (int i = 0; i < LENGTH; i++) {
			hashval = (hashval * PRIME_32B) ^ s.charAt(i);
		}
		if (fold == 0)
			return hashval;
		return (hashval ^ (hashval >> fold)) & mask;
	}

	/**
	 * Same as the function above but this time it uses UUID.
	 * @return Hash value.
	 */
	protected static long generateHash() {
		String s = UUID.randomUUID().toString();
		long hashval = OFFSET_BASIS_32B;
		final int LENGTH = s.length();
		for (int i = 0; i < LENGTH; i++) {
			hashval = (hashval * OFFSET_BASIS_32B) ^ s.charAt(i);
		}
		return (hashval ^ (hashval >> FOLD)) & MASK;
	}
}
