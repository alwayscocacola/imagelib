import org.apache.log4j.Logger;

import java.io.File;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

	private static final Logger logger = Logger.getLogger(Main.class);

	private static final int POOL_SIZE = 10;

	/**
	 * Runs the program.
	 * @param executorService
	 */
	private static void run(ExecutorService executorService) {
		logger.info("Main.run()");
		File[] files = new File(ImageSettings.ORIGINAL_PATH).listFiles();
		assert files != null;
		for (File file : files) {
			if (!file.isHidden()) {
				Runnable worker = new ImageProcessorWorker(file);
				executorService.execute(worker);
			}
		}
		executorService.shutdown();
		try {
			executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) {
			logger.debug(e.getMessage());
		}
		logger.info("shutdown");
	}

	public static void main(String[] args) {
		ExecutorService executorService = Executors.newFixedThreadPool(POOL_SIZE);
		logger.info("Run Main");

		run(executorService);

		logger.info("terminated");
	}
}
