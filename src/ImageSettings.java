/**
 * Image settings such as paths and maximum width/height for different
 * thumbnail sizes.
 */

public class ImageSettings {

	protected static final String ORIGINAL_PATH = "";
	protected static final String IMG_MEDIUM_SIZE_PATH = "";
	protected static final String IMG_BIG_THUMBNAIL_PATH = "";
	protected static final String IMG_MEDIUM_THUMBNAIL_PATH = "";
	protected static final String IMG_SMALL_THUMBNAIL_PATH = "";

	protected static final int MEDIUM_WIDTH = 804;
	protected static final int MEDIUM_HEIGHT = 554;

	protected static final int BIG_THUMBNAIL_WIDTH = 320;
	protected static final int BIG_THUMBNAIL_HEIGHT = 222;

	protected static final int MEDIUM_THUMBNAIL_WIDTH = 220;
	protected static final int MEDIUM_THUMBNAIL_HEIGHT = 152;

	protected static final int SMALL_THUMBNAIL_WIDTH = 140;
	protected static final int SMALL_THUMBNAIL_HEIGHT = 97;

}
