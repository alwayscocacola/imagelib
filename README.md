# ImageLib

This is a threaded image processor. It simply resize images in the given directory and creates some thumbnails and *removes* the original images.

It's written completely in Java and doesn't have any dependencies except log4j.

# License

Released under MIT license. <http://opensource.org/licenses/MIT>